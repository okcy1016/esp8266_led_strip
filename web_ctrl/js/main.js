var mqttClient = mqtt.connect('mqtt://xxx.xxx.xxx.xxx:xxxx')


function PushColorValue(picker) {
    document.getElementsByTagName('body')[0].style.color = '#' + picker.toString();

    // Publishing
    mqttClient.publish("hex_color_value", "#"+picker.toString());
}

function TurnOffAllLEDs() {
    // Publishing
    mqttClient.publish("hex_color_value", "#000000");
}
