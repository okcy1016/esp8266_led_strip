import utime

def TurnOff(np):
    for _ord in range(np.n):
        np[_ord] = (0, 0, 0)

    np.write()
    
def InitLEDs():
    import machine, neopixel
    return neopixel.NeoPixel(machine.Pin(4), 30)

def LoopWaitForInstructions(np):
    from umqtt.simple import MQTTClient
    c = MQTTClient("esp8266_led_strip", "119.29.136.188", 1508)

    # Cool lambda here!
    c.set_callback(lambda topic, msg: SetLEDColor(np, topic, msg))
                   
    c.connect()
    
    # Blink first led when connect successfully
    BlinkLED(np)
    
    c.subscribe(b"hex_color_value")

    while True:
        # None-blocking check message
        c.check_msg()
        utime.sleep_ms(100)

    c.disconnect()

    
def SetLEDColor(np, topic, msg):
    if topic.decode() == "hex_color_value":
        rgbValue = HEXToRGB(msg.decode())
        SetLEDsByRGB(np, rgbValue)


def HEXToRGB(hexColorValue):
    tmpValue = hexColorValue.lstrip("#")
    return tuple(int(tmpValue[i:i+2], 16) for i in (0, 2 ,4))
        

def SetLEDsByRGB(np, rgbValue):
    for _ord in range(np.n):
        np[_ord] = rgbValue

    np.write()
        

def BlinkLED(np):
    for _ in range(3):
        np[0] = (16, 0, 0)
        np.write()
        utime.sleep_ms(500)
        np[0] = (0, 0, 0)
        np.write()
        utime.sleep_ms(500)

def IllumeLEDsForDailyUse(np):
    led_illume_list = [6, 7, 14, 15, 22, 23]
    for i in led_illume_list:
        np[i] = (110, 69, 40)
    np.write()

if __name__ == "__main__":
    print("hello world!")

    # Initiate LED strip
    np = InitLEDs()
    
    # Blink the first led to indicate boot is completed
    BlinkLED(np)

    IllumeLEDsForDailyUse(np)
    
    # Wait 10 seconds for aid of webrepl
    utime.sleep(10)
    
    # LoopWaitForInstructions(np)
