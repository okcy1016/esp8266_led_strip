def do_connect():
    import network
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect('PandoraBox-2.4G-ECCAE0', 'hahahaha')

    while not wlan.isconnected():
        pass
    
    print('network config:', wlan.ifconfig())


if __name__ == "__main__":
    # turn off both AP or STA mode
    import network
    sta_if = network.WLAN(network.STA_IF)
    ap_if = network.WLAN(network.AP_IF)
    sta_if.active(False)
    ap_if.active(False)
    
    # Connect WiFi firstly
    # do_connect()
# import webrepl
# webrepl.start()
